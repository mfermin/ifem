  % Shell program for solving FE problems using numerical integration
%
%
%
% Build "manually" a 2D mesh of triangles over a rectangle.
%
%clear all
nsd=2; % este programa sólo funciona en 2 dimensiones
a=1;
b=1;
nx=10;
ny=10;
dx=a/nx;
dy=b/ny;

stress= 1; % flag stress=1 / strain=0
nu=0.38;% ratio Poisson
E =10000; %elastic modul Young
% Evaluate forces
force=@(x,y)[0; 0];

%
% Grid:
%
Nel=nx*ny*2;
Nod=(nx+1)*(ny+1);
coordinates=zeros(Nod,2);
in=0;
for iy=0:ny
  for ix=0:nx
    in++;
    coordinates(in,1)=ix*dx;
    coordinates(in,2)=iy*dy;
  end
end
elements=zeros(Nel,3);
ie=0;
for iy=1:ny
  for ix=1:nx
    n1=ix+(nx+1)*(iy-1);
    n2=n1+1;
    n3=n1+(nx+1);
    n4=n3+1;
    ie++;
    elements(ie,1)=n1;
    elements(ie,2)=n2;
    elements(ie,3)=n3;
    ie++;
    elements(ie,1)=n2;
    elements(ie,2)=n4;
    elements(ie,3)=n3;
  end
end
%
% At least one side witch Dirichlet conditions in each direction
dirichletx=dirichlety=zeros(1,2); % node1 , node2 of each side with DC
idx=idy=0;
% side y=0  % dirichlet condition in "x" and "y" directions
n1=1;
for ix=1:nx
  n2=n1+1;
  idx++;
  dirichletx(idx,1)=n1;
  dirichletx(idx,2)=n2;
  idy++;
  dirichlety(idy,1)=n1;
  dirichlety(idy,2)=n2;
  n1=n2;
end

% side x=a  % dirichlet condition only in "x" direction
for iy=1:ny
  n2=n1+nx+1;
  idx++;
  dirichletx(idx,1)=n1;
  dirichletx(idx,2)=n2;
  n1=n2;
end
% side y=b % no dirichlet condition
for ix=1:nx
  n2=n1-1;
  %idy++;
  %dirichlety(idy,1)=n1;
  %dirichlety(idy,2)=n2;
  n1=n2;
end
% side x=0  % dirichlet condition only in "x" direction
for iy=1:ny
  n2=n1-(nx+1);
  idx++; 
  dirichletx(idx,1)=n1;
  dirichletx(idx,2)=n2;
  n1=n2;
end
%
% Build Matrix and RHS
%
%TEST
%nu=0.3; E=1992900;%para test calculado a mano
%elements=[ 1 2 3]; coordinates=[4.0, 1.0 ; 3.0, 3.0; 2.0, 1.0]%para test calculado a mano
[K, M, F, B, D]=assElastic(coordinates, elements, 2, nu, E, stress, force);
%u=[0.002 0 0.0005 0.002 0.001 -0.0005]'; %para test calculado a mano
%[Se, vonMss] = pprocessEDT (coordinates, elements, B, D,nu,u, stress) %para test calculado a mano

% Define unknowns vector
u = sparse ( 2*size(coordinates,1), 1 );
% Set Dirichlet Boundary Conditions (DBC)
% nodes with DBC in x direction
BoundNodesx = unique ( dirichletx );
% nodes with DBC in y direction
BoundNodesy = unique ( dirichlety );
BoundUnks=[BoundNodesx; BoundNodesy+Nod]; % list of unknowns with DBC
%nDBC=size(BoundUnks,1);
% Homogeneous
%u(BoundUnks) = zeros(BoundUnks,1);
% x displacement unknowns: starting at 1:
u(BoundNodesx)=u_bx(coordinates(BoundNodesx,:));
% y displacement unknowns: starting at Nod+1;
u(BoundNodesy+Nod)=u_by(coordinates(BoundNodesy,:));

% Should only be necessary with non-homogeneous b.c.
F = F - K * u; 

% Determine free nodes
FreeUnks=setdiff ( 1:2*size(coordinates,1), BoundUnks );
#FreeUnks = vectFree(1:2);
% Solve system of equations for the free nodes
u(FreeUnks) = K(FreeUnks,FreeUnks) \ F(FreeUnks);

% Show solution
elements4=[];

uxy=[u(1:Nod), u(1+Nod:2*Nod)]';  
%postprocessing 
[Se, SvM] = pprocessEDT (coordinates, elements, B, D,nu,uxy, stress)

SSe=zeros(Nod,nsd);

fprintf('tensión máxima: (%8.3e) de vonMises en el Nodo: %d.\n',...
            max(SvM(3,:)),find(SvM==max(SvM(3,:))));

           
mu = E/(2*(1+nu)); lambda = E*nu/((1+nu)*(1-2*nu));
% Representation of the solution
[AvE,Eps3,Eps4,AvS,Sigma3,Sigma4] = ...  %AvS(1,:) ->Se(1,:) | AvS(2:3,:) ->Se(3,:) | AvS(4,:) ->Se(2,:)
    avmatrix(coordinates,elements,elements4,uxy,lambda,mu);
show2D(elements,elements4,coordinates,AvS,uxy,lambda,mu,Nod);
title("Tensión");

%d_show=SvM(1,:);  % first principal stress
%d_show=SvM(2,:); % second principal stress
%d_show=SvM(3,:);  % vonMises principal stress
d_show = sqrt(uxy(1,:).^2 + uxy(2,:).^2);  %total displacement
show2D_disp(coordinates',elements,uxy,d_show',1,0)
title("Desplazamiento");
