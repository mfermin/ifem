## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} pprocessEDT (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

function [gp_sse, SvM] = pprocessEDT (coordinates, elements, B, D,nu,u, stress)
  
  Nod=size(coordinates,1);
  %create deformation vector
  Nel=size(elements,1)
  nen=size(elements,2)
  e = zeros(3,Nel); %epsilon
%create tensiones vector [Sex, Sey, Txy]
  Se = zeros(3,Nel);
  gp_sse=zeros(3,Nod); % stress at gauss points
  w_sse=zeros(1,Nod);
  SvM=zeros(3,Nod);

 for j = 1 : Nel  
   u_xy=[u(:,elements(j,1))', u(:,elements(j,2))', u(:,elements(j,3))']' %displacement for nodes in elements
   e(:,j) = B(:,:,j)*u_xy; % total strain  {e} = [B]{u}  =dNd
   Se(:,j) = D(:,:)*e(:,j); % elastic stress  {\sigma}= [D]{e}
   #Bt=B(:,:,j)'*Se(:,j);
    for i=1:nen
      gp_sse(:,elements(j,i))  =gp_sse(:,elements(j,i))+Se(:,j); 
      w_sse(1,elements(j,i))  =w_sse(1,elements(j,i))+1;
    endfor
end

for i=1:Nod
  gp_sse(:,i) = gp_sse(:,i)/w_sse(1,i);
  
  %calculate vonMises
  SvM(1,i) = 0.5*(gp_sse(1,i)+gp_sse(2,i)) + sqrt( (0.5*(gp_sse(1,i)-gp_sse(2,i))).^2 + gp_sse(3,i).^2); % first principal stress
  SvM(2,i) = 0.5*(gp_sse(1,i)+gp_sse(2,i)) - sqrt( (0.5*(gp_sse(1,i)-gp_sse(2,i))).^2 + gp_sse(3,i).^2);  % second principal stress
  SvM(3,i) = sqrt( SvM(1,i).^2 + SvM(1,i).^2 - SvM(1,i).*SvM(1,i) );                                                           % plane-stress case
endfor
                    
endfunction
