function [ub] = u_bx (coordinates)
  nod=size(coordinates,1)
  ub=zeros(nod,1);
  for i=1:nod
    x=coordinates(i,1);
    y=coordinates(i,2);
    ub(i)=x;
  end
end