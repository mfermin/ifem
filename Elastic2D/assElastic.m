%
%  Esta función ensambla una matriz y un RHS en una malla de simplices n-D
%  Además de calcular un campo escalar, calcula su gradiente
%
%  Argumentos:
%  * coordinates: matriz de coordenadas de la malla
%  (nnodos x dimensiones)
%  * elements: nodos de cada elemento
%  (nelementos x (dimensiones + 1))
%  * degree: grado del polinomio a integrar exactamente, determina
%  la cuadratura a utilizar.
%
function [K, M, F, B, D] = assElastic(coordinates, elements, degree, nu, E, stress, force)
  pkg load symbolic
  syms y; syms x;
% spatial dimensions
  nsd=size(coordinates,2)
% number of nodes per element: nsd+1 for simplices
  nnpe=nsd+1;
  nod=size(coordinates,1)
  if (size(elements,2) != nsd+1)
    "error in size(elements)"
  endif
%
% Select quadrature according to nsd and degree of polynomial integrated exactly
%
  if (nsd == 1)
% quadrature rules in 1D: Gauss-Legendre ([-1, 1])
    if (degree <=1 )
      weights=load("1D/gl_d1_o1_w.txt");
      gpoints=load("1D/gl_d1_o1_x.txt");
    elseif (degree <= 3)
      weights=load("1D/gl_d1_o2_w.txt");
      gpoints=load("1D/gl_d1_o2_x.txt");
    elseif (degree <= 5)
      weights=load("1D/gl_d1_o3_w.txt");
      gpoints=load("1D/gl_d1_o3_x.txt");
    elseif (degree <= 7)
      weights=load("1D/gl_d1_o4_w.txt");
      gpoints=load("1D/gl_d1_o4_x.txt");
    elseif (degree <= 9)
      weights=load("1D/gl_d1_o5_w.txt");
      gpoints=load("1D/gl_d1_o5_x.txt");
    else
      weights=load("1D/gl_d1_o5_w.txt");
      gpoints=load("1D/gl_d1_o5_x.txt");
      "Degree too high ! (1D -> 9)"
    endif
% [-1, 1] -> [0, 1]
    weights=0.5*weights;
    gpoints=0.5*(gpoints+1);
  elseif (nsd == 2)
% Quadrature rules in triangles, see:
% http://people.sc.fsu.edu/~jburkardt/datasets/quadrature_rules_tri/quadrature_rules_tri.html
    if (degree <=1 )
      weights=load("2D/centroid_w.txt");
      gpoints=load("2D/centroid_x.txt");
    elseif (degree <= 2)
      weights=load("2D/strang1_w.txt");
      gpoints=load("2D/strang1_x.txt");
    elseif (degree <= 3)
      weights=load("2D/strang3_w.txt");
      gpoints=load("2D/strang3_x.txt");
    elseif (degree <= 4)
      weights=load("2D/strang5_w.txt");
      gpoints=load("2D/strang5_x.txt");
    elseif (degree <= 5)
      weights=load("2D/strang7_w.txt");
      gpoints=load("2D/strang7_x.txt");
    elseif (degree <= 6)
      weights=load("2D/strang8_w.txt");
      gpoints=load("2D/strang8_x.txt");
    elseif (degree <= 7)
      weights=load("2D/strang10_w.txt");
      gpoints=load("2D/strang10_x.txt");
    elseif (degree <= 8)
      weights=load("2D/toms584_19_w.txt");
      gpoints=load("2D/toms584_19_x.txt");
    elseif (degree <= 9)
      weights=load("2D/toms612_19_w.txt");
      gpoints=load("2D/toms612_19_x.txt");
    elseif (degree <= 11)
      weights=load("2D/toms612_28_w.txt");
      gpoints=load("2D/toms612_28_x.txt");
    elseif (degree <= 13)
      weights=load("2D/toms706_37_w.txt");
      gpoints=load("2D/toms706_37_x.txt");
    else
      weights=load("2D/toms706_37_w.txt");
      gpoints=load("2D/toms706_37_x.txt");
      "Degree too high ! (2D -> 13)"
    endif
  elseif (nsd == 3)
%  Quadrature rules in tetrahedra, see:
%  http://people.sc.fsu.edu/~jburkardt/datasets/quadrature_rules_tet/quadrature_rules_tet.html
    if (degree <= 1)
      weights=load("3D/keast0_w.txt");
      gpoints=load("3D/keast0_x.txt");
    elseif (degree <= 2)
      weights=load("3D/keast2_w.txt");
      gpoints=load("3D/keast2_x.txt");
    elseif (degree <= 3)
      weights=load("3D/keast3_w.txt");
      gpoints=load("3D/keast3_x.txt");
    elseif (degree <= 4)
      weights=load("3D/keast4_w.txt");
      gpoints=load("3D/keast4_x.txt");
    elseif (degree <= 5)
      weights=load("3D/keast6_w.txt");
      gpoints=load("3D/keast6_x.txt");
    elseif (degree <= 6)
      weights=load("3D/keast7_w.txt");
      gpoints=load("3D/keast7_x.txt");
    elseif (degree <= 7)
      weights=load("3D/keast8_w.txt");
      gpoints=load("3D/keast8_x.txt");
    elseif (degree <= 8)
      weights=load("3D/keast9_w.txt");
      gpoints=load("3D/keast9_x.txt");
    else
      weights=load("3D/keast9_w.txt");
      gpoints=load("3D/keast9_x.txt");
      "Degree too high ! (2D -> 13)"
    endif
  else
% dimensions > 3: baricenter quadrature (degree 1)
    if (degree <= 1)
      weights=1;
      gpoints=ones(1,nsd)/(nsd+1);
    else
      weights=1;
      gpoints=ones(1,nsd)/(nsd+1);
      "Degree too high ! (nD -> 1)"
    endif
  endif
%
% Define global Sub matrices and RHSides
%
% Stiffness matrix 
  K=sparse(nsd*nod,nsd*nod);
% Mass matrix   
  M=sparse(nsd*nod,nsd*nod); 
%load vector
  F=zeros(nsd*nod,1); 
% create gradient matrix
  B = zeros(3,6,size(elements, 1)); % gradient matrix 
%%D matrix for stress
  D = zeros(nsd+1,nsd+1); % D matrix // 
                    
%
% form functions 2D:
%  f1=@(x) (1-x(1)-x(2));
%  f2=@(x) x(1);
%  f3=@(x) x(2);
% column vector of form functions (n-D) needs column vector in argument
  f=@(x)[1-sum(x);x];
%
% RHS function
%
  source=@(x) nsd*pi*pi*prod(sin(pi*x));
%  source=@(x) 2.0;
%
% Loop over elements
%
%material anisotropico 2D {σ_z = ε_z=0}
%D(:,:) = E/(1 - nu^2)* [ 1  nu      0          0          0;
%                                    nu  1       0          0          0;
%                                      0   0 (1-nu)/2     0          0;
%                                      0   0       0     (1-nu)/2    0;
%                                      0   0       0         0   (1-nu)/2;];

%material isotropico 2D {σ_z = σ_xz = σ_yz = 0}
if (stress ==1)   %build D elastic matrix: for stress
  D(:,:)  =E/((1 + nu)*(1 - 2*nu))* [1 - nu    nu         0 ; ...  
                                                             nu  1 - nu       0 ; ...
                                                           0          0    (1 - 2*nu)/2];  
                                                           
else  %build D elastic matrix: for strain
   D(:,:) = E/(1 - nu^2)* [ 1  nu      0;
                                        nu  1        0;
                                        0   0  (1-nu)/2];
endif


%mu=E/(2*(1+nu)); lambda=E*nu/((1+nu)*(1-2*nu));
%DD=mu*[2 0 0; 0 2 0; 0 0 1]+lambda*[1 1 0; 1 1 0; 0 0 0];

for j = 1 : size(elements, 1)
  %
  % Vertices of the element
  %
        vertices = coordinates(elements(j,:),:);
  %
  % Transformation matrix  (x_mesh = T * x_master + r0)
      r0=vertices'(:,1);
      T=vertices'(:,2:end)-r0*ones(1,nsd);
  %

  %element_vert
  v1x=vertices'(1,1); v1y=vertices'(2,1);
  v2x=vertices'(1,2); v2y=vertices'(2,2);
  v3x=vertices'(1,3); v3y=vertices'(2,3);
    
  %Area:
  A = 0.5*det([1 v1x v1y; % Area of element
                       1 v2x v2y;
                       1 v3x v3y]);
if(0)      
%tic;
  %vertices i=1,j=2,k=3  %warning: passing floating-point values to sym
  fN1=1/(2*A)*((v2x*v3y - v3x*v2y)+(v2y-v3y)*x +(v3x-v2x)*y) 
  fN2=1/(2*A)*((v3x*v1y - v1x*v3y)+(v3y-v1y)*x +(v1x-v3x)*y) 
  fN3=1/(2*A)*((v1x*v2y - v2x*v1y)+(v1y-v2y)*x +(v2x-v1x)*y)  
  
  %ei=elements(j,1); ej=elements(j,2); ek=elements(j,3);
  % Calculate matrix B coefficients 2->y
  %vfn1=fN1(1); % gauss_point
  b1 = double(diff(fN1, 'x'));
  b2 = double(diff(fN2, 'x'));
  b3 = double(diff(fN3, 'x')); 
  c1 = double(diff(fN1, 'y'));
  c2 = double(diff(fN2, 'y'));
  c3 = double(diff(fN3, 'y'));
  %strain matrix
   B(:,:,j) = [b1  0  b2  0   b3  0; 
                     0   c1 0  c2  0   c3;
                    c1  b1 c2 b2 c3 b3];
                   
%t1 = toc;
%fprintf('El proceso ha tardado %d segundos', t1);
endif
  %valuate force each points
  %Load vector: [u(i), v(i), u(j), v(j), u(k), v(k)]
# if(j>5 && j<22)
    fk= [force(v1x, v1y)(1), force(v1x, v1y)(2),...
           force(v2x, v2y)(1), force(v2x, v2y)(2),...
           force(v3x, v3y)(1), force(v3x, v3y)(2)]';
   #else      
     # fk=[0,0,0,0,0,0]';
   #endif
  % element size
  %
   esize=det([ ones(1,nsd+1); vertices' ])/prod(1:nsd);
  %
  % Transform master->mesh (n-D) row vectors of vertices, return column vector
  %
      xmesh=@(x) vertices' * f(x);
  % Elementary stiffness matrix and RHS
      Be=zeros(nsd+1,1);

  % Elementary mass matrix
      MK=zeros(2*(nsd+1),2*(nsd+1));
  %
  % Swaps Gauss points
  %
  % tic;
  for ipg=1:size(weights,1)
 
  %
  % Coordinates of this Gauss point
  %
        xg=gpoints(ipg,:)';
  %
  % Form functions evaluated in this gauss point
  %
        pgp=f(xg); % gauss_point
  %
  % Gradient of the form functions in this gauss point
  %     gpgp(i,j) = \frac{ \partial \phi_j }{ \partial x_i }
  %
        gpgp=T'\[-1*ones(nsd,1), eye(nsd,nsd)];
   %
      % Swaps rows of the elementary matrices
      %
       %strain matrix
       B(:,:,j)=[ mod(1:6,2) .* reshape(gpgp,1,6) ; mod((1:6)+1,2) .* reshape(gpgp,1,6) ; ...
       circshift(mod(1:6,2) .* reshape(gpgp,1,6),[0,1]) + circshift(mod((1:6)+1,2) .* reshape(gpgp,1,6),[0,-1])];
      %
      for ir=1:nnpe
      %
      % Swaps columns of the elementary matrices
      %
        for jr=1:nnpe
        % Elementary Mass matrix: MKfK [6x6]
        % \phi_{gauss_point} x \phi_{gauss_point}  
        %  %Matrix Elastic mass
        %MK=[2 0 1 0 1 0; 0 2 0 1 0 1; 1 0 2 0 1 0; 0 1 0 2 0 1; 1 0 1 0 2 0; 0 1 0 1 0 2]*A/12;
        if((-1)^(jr+ir) ==1)
            MK(ir,jr)=MK(ir,jr)+...
                     pgp(ir)' * pgp(jr) * weights(ipg)*esize; 
            MK(ir+3,jr+3)=MK(ir+3,jr+3) + ...
                     pgp(ir)' * pgp(jr) * weights(ipg)*esize;
        else
            MK(ir,jr+3)=MK(ir,jr+3) + ...
                     pgp(ir)' * pgp(jr) * weights(ipg)*esize;
            MK(ir+3,jr)=MK(ir+3,jr) + ...
                     pgp(ir)' * pgp(jr) * weights(ipg)*esize;
        endif
       end
      end
      % Elementary RHS
      %
      %for ir=1:nnpe
       % Be(ir)=Be(ir)+source(xmesh(xg))*pgp(ir)*weights(ipg)*esize;
     %end
  endfor % Gauss points
        %t2=toc;
   %  fprintf('El proceso ha tardado %d segundos \n', t2);
    % fprintf('P2 es %d mas rápido\n', t1/t2);  
   % Elementary stiffness matrix: KK [6x6] =[B]’*[D]*[B]*area 
  KK = B(:,:,j)'*D(:,:)*B(:,:,j)*A;
  % element load vector FK[6x1]
  FK=MK*fk; 
  
  % Assemble elementary matrices in global matrices
  %  elementary RHS in global RHS
  np=elements(j,:);
%  I=[np, np+nod];
  I=reshape([np; np+nod],1,6);
  %return:
  K(I,I)=K(I,I)+KK;   % Global stiffness matrix [2*nodes, 2*nodes] 
  M(I,I)=M(I,I)+MK; % Global mass matrix [2*nodes, 2*nodes] 
  F(I)=F(I)+FK;                       % Global load vector [2*nodes,1]   
endfor % Elements of the mesh
%
endfunction
